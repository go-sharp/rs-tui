package main

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

func NewDialog(title string) *Dialog {
	d := &Dialog{
		Flex:   tview.NewFlex(),
		text:   tview.NewTextView(),
		footer: tview.NewFlex(),
		spacer: tview.NewBox(),
	}

	d.footer.SetDirection(tview.FlexColumn)
	d.text.SetBorderPadding(1, 1, 1, 1)

	cbox := tview.NewFlex().AddItem(d.spacer, 0, 1, false).
		AddItem(d.footer, 0, 8, false).
		AddItem(d.spacer, 0, 1, false)

	d.SetDirection(tview.FlexRow)
	d.AddItem(d.text, 0, 3, false)
	d.AddItem(cbox, 1, 0, false)
	d.text.SetTextAlign(tview.AlignCenter)
	d.SetBorder(true).SetTitle(title)

	return d
}

// Dialog represents a simple message dialog.
type Dialog struct {
	*tview.Flex
	text     *tview.TextView
	footer   *tview.Flex
	buttons  []*tview.Button
	spacer   *tview.Box
	selected int
}

func (d *Dialog) Draw(screen tcell.Screen) {
	d.Flex.Draw(screen)
}

func (d *Dialog) SetBackgroundColor(color tcell.Color) *tview.Box {
	d.Flex.SetBackgroundColor(color)
	d.text.SetBackgroundColor(color)
	d.footer.SetBackgroundColor(color)
	d.spacer.SetBackgroundColor(color)

	return d.Box
}

func (d *Dialog) SetText(text string) *Dialog {
	d.text.SetText(text)
	return d
}

func (d *Dialog) SetTextColor(color tcell.Color) *Dialog {
	d.text.SetTextColor(color)
	return d
}

func (d *Dialog) Focus(delegate func(p tview.Primitive)) {
	handler := func(key tcell.Key) {
		switch key {
		case tcell.KeyBacktab:
			d.selected--
			if d.selected < 0 {
				d.selected = len(d.buttons) - 1
			}
		case tcell.KeyTab:
			d.selected++
			if d.selected >= len(d.buttons) {
				d.selected = 0
			}
		}
		d.Focus(delegate)
	}

	if len(d.buttons) > 0 && d.selected < len(d.buttons) {
		d.buttons[d.selected].SetBlurFunc(handler)
		delegate(d.buttons[d.selected])
	} else {
		d.Flex.Focus(delegate)
	}
}

func (d *Dialog) HasFocus() bool {
	if len(d.buttons) > 0 && d.selected < len(d.buttons) {
		return d.buttons[d.selected].HasFocus()
	}
	return d.HasFocus()
}

func (d *Dialog) RemoveAllButton() {
	for i := range d.buttons {
		d.footer.RemoveItem(d.buttons[i]).
			RemoveItem(d.spacer)
		d.buttons[i] = nil
	}

	d.buttons = d.buttons[:0]
	d.selected = 0
}

func (d *Dialog) RemoveButton(name string) {
	var buttons []*tview.Button
	for i := range d.buttons {
		if d.buttons[i].GetLabel() != name {
			buttons = append(buttons, d.buttons[i])
		}
	}

	d.RemoveAllButton()
	for _, b := range buttons {
		d.addButton(b)
	}
}

func (d *Dialog) AddButton(name string, handler func()) *Dialog {
	d.addButton(tview.NewButton(name).SetSelectedFunc(handler))
	return d
}

func (d *Dialog) addButton(btn *tview.Button) *Dialog {
	d.buttons = append(d.buttons, btn)
	d.footer.AddItem(btn, 0, 1, false)
	d.footer.AddItem(d.spacer, 1, 0, true)

	return d
}
