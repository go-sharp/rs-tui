module bitbucket.org/go-sharp/rs-tui

require (
	github.com/gdamore/tcell v1.1.1
	github.com/google/pprof v0.0.0-20190109223431-e84dfd68c163 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	github.com/rivo/tview v0.0.0-20181226202439-36893a669792
	golang.org/x/arch v0.0.0-20181203225421-5a4828bb7045 // indirect
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/sys v0.0.0-20190115152922-a457fd036447 // indirect
)
