package main

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// center returns a new primitive which shows the provided primitive in its
// center, given the provided primitive's size.
func center(width, height int, p tview.Primitive) tview.Primitive {
	return centerColored(width, height, tcell.ColorDefault, p)
}

func centerColored(width, height int, color tcell.Color, p tview.Primitive) tview.Primitive {
	box := tview.NewBox().SetBackgroundColor(color)
	return tview.NewFlex().
		AddItem(box, 0, 1, false).
		AddItem(tview.NewFlex().
			SetDirection(tview.FlexRow).
			AddItem(box, 0, 1, false).
			AddItem(p, 0, height, true).
			AddItem(box, 0, 1, false), 0, width, true).
		AddItem(box, 0, 1, false)
}
