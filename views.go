package main

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

const (
	cmdTitle     = " Rescue System V1.0.0 "
	pageMenu     = "MenuPage"
	pageStatus   = "StatusPage"
	pageDialog   = "DialogPage"
	pagePassword = "PasswordDialog"
)

func NewMainView() MainView {
	return MainView{
		app:     tview.NewApplication(),
		pages:   tview.NewPages(),
		menulst: tview.NewList(),
		dialog:  NewDialog(" Dialog "),
	}
}

type MainView struct {
	app          *tview.Application
	pages        *tview.Pages
	menulst      *tview.List
	dialog       *Dialog
	systemStatus Status
	visiblePage  string
}

func (m *MainView) Show() error {

	var password string
	var repeat string
	m.pages.AddPage(pagePassword, center(3, 3, tview.NewForm().
		AddPasswordField("Password", "", 40, '*', func(str string) {
			password = str
		}).
		AddPasswordField("Repeat Password", "", 40, '*', func(str string) {
			repeat = str
		}).
		AddButton("Change", func() {
			if password == repeat && len(password) > 4 {
				m.ShowMsg("Password successfully changed!", MsgTypeSuccess, func(i int) {
					m.switchToPage(pageMenu)
				}, "Okay")
			} else {
				m.ShowMsg("Invalid password supplied! "+password, MsgTypeError, func(i int) {}, "Okay")
			}
		}).
		AddButton("Cancel", func() { m.switchToPage(pageMenu) })), true, false)
	m.pages.AddPage(pageDialog, center(8, 8, m.dialog), true, false)
	m.initStatusPage()
	m.initMenu()
	m.pages.SetTitle(cmdTitle).
		SetBorder(true)

	return m.app.SetRoot(center(3, 3, m.pages), true).Run()
}

func (m *MainView) ShowMsg(msg string, typ MsgType, btnClicked func(index int), buttons ...string) {
	m.dialog.SetText(msg)
	switch typ {
	case MsgTypeError:
		m.dialog.SetTitle(" Error ")
		m.dialog.SetBackgroundColor(tcell.ColorRed)
	case MsgTypeWarn:
		m.dialog.SetTitle(" Warning ")
		m.dialog.SetBackgroundColor(tcell.ColorOrange)
	case MsgTypeSuccess:
		m.dialog.SetTitle(" Success ")
		m.dialog.SetBackgroundColor(tcell.ColorGreen)
	default:
		m.dialog.SetTitle(" Info ")
		m.dialog.SetBackgroundColor(tcell.ColorYellow)
	}

	currentPage := m.visiblePage
	m.dialog.RemoveAllButton()

	for i, btn := range buttons {
		j := i
		m.dialog.AddButton(btn, func() {
			m.switchToPage(currentPage)
			btnClicked(j)
		})
	}

	m.switchToPage(pageDialog)
}

func (m *MainView) initStatusPage() {
	sv := NewStatusView()
	sv.SetOkayFunc(func() {
		if m.systemStatus == StatusLocked {
			m.ShowMsg("Do you want to unlock the system and resume normal operation?",
				MsgTypeWarn, func(idx int) {
					if idx == 0 {
						sv.SetStatus(StatusUnlocked)
						m.systemStatus = StatusUnlocked
					}
				}, "Yes", "No")
		} else {
			m.ShowMsg("Do you want to lock the system and switch to limited operation?",
				MsgTypeWarn, func(idx int) {
					if idx == 0 {
						sv.SetStatus(StatusLocked)
						m.systemStatus = StatusLocked
					}
				}, "Yes", "No")
		}
	}).SetCancelFunc(func() {
		m.switchToPage(pageMenu)
	})

	m.pages.AddPage(pageStatus, center(8, 8, sv), true, false)
}

func (m *MainView) initMenu() {
	m.pages.AddPage(pageMenu, center(1, 1, m.menulst), true, true)
	m.visiblePage = pageMenu

	m.menulst.AddItem("Change Password", "", 'c', func() {
		m.switchToPage(pageStatus)
	})
	m.menulst.AddItem("Reset System", "", 'r', func() {
		m.switchToPage(pagePassword)
	})
	m.menulst.AddItem("Exit", "", 'x', func() { m.app.Stop() })
}

func (m *MainView) switchToPage(page string) {
	m.pages.SwitchToPage(page)
	m.app.SetFocus(m.pages)
	m.visiblePage = page
}

type MsgType int

const (
	MsgTypeInfo MsgType = iota
	MsgTypeSuccess
	MsgTypeWarn
	MsgTypeError
)
