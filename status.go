package main

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

type Status int

const (
	StatusLocked Status = 1 << iota
	StatusUnlocked
)

type StatusView struct {
	*tview.Box
	grid       *tview.Grid
	status     *tview.TextView
	cancelFunc func()
	okayFunc   func()
	okayBtn    *tview.Button
	cancelBtn  *tview.Button
	focused    *tview.Button
}

func NewStatusView() *StatusView {
	sv := &StatusView{
		Box:       tview.NewBox(),
		grid:      tview.NewGrid(),
		status:    tview.NewTextView(),
		okayBtn:   tview.NewButton("Lock"),
		cancelBtn: tview.NewButton("Cancel"),
	}

	sv.okayBtn.SetSelectedFunc(func() {
		if sv.okayFunc != nil {
			sv.okayFunc()
		}
	})
	sv.cancelBtn.SetSelectedFunc(func() {
		if sv.cancelFunc != nil {
			sv.cancelFunc()
		}
	})

	sv.focused = sv.cancelBtn
	sv.status.SetTextAlign(tview.AlignCenter).
		SetTextColor(tcell.ColorWhite)
	sv.grid.SetRows(0, 1, 1, 0).
		SetColumns(0, 20, 20, 0)
	sv.grid.SetGap(2, 2)
	sv.grid.AddItem(tview.NewTextView().SetText("System Status:"), 1, 1, 1, 1, 1, 1, false)
	sv.grid.AddItem(sv.status, 1, 2, 1, 1, 1, 1, false)
	sv.grid.AddItem(sv.okayBtn, 2, 1, 1, 1, 1, 1, false)
	sv.grid.AddItem(sv.cancelBtn, 2, 2, 1, 1, 1, 1, false)

	sv.SetStatus(StatusUnlocked)

	return sv
}

func (s *StatusView) Draw(screen tcell.Screen) {
	s.Box.Draw(screen)
	s.grid.SetRect(s.GetInnerRect())
	s.grid.Draw(screen)
}

func (s *StatusView) SetStatus(status Status) {
	if status == StatusLocked {
		s.status.SetText("Locked")
		s.okayBtn.SetLabel("Unlock")
		s.status.SetBackgroundColor(tcell.ColorRed)
		return
	}

	s.status.SetText("Unlocked")
	s.okayBtn.SetLabel("Lock")
	s.status.SetBackgroundColor(tcell.ColorGreen)
}

func (s *StatusView) SetOkayFunc(fn func()) *StatusView {
	s.okayFunc = fn
	return s
}

func (s *StatusView) SetCancelFunc(fn func()) *StatusView {
	s.cancelFunc = fn
	return s
}

func (s *StatusView) Focus(delegate func(p tview.Primitive)) {
	handler := func(key tcell.Key) {
		switch key {
		case tcell.KeyESC:
			if s.cancelFunc != nil {
				s.cancelFunc()
				return
			}
		case tcell.KeyBacktab:
			fallthrough
		case tcell.KeyTab:
			if s.focused == s.okayBtn {
				s.focused = s.cancelBtn
			} else {
				s.focused = s.okayBtn
			}
			s.Focus(delegate)
		}
	}

	s.focused.SetBlurFunc(handler)
	delegate(s.focused)
}

func (s *StatusView) HasFocus() bool {
	return s.focused.HasFocus()
}
