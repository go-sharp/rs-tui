package main

import "fmt"

func main() {
	app := NewMainView()
	if err := app.Show(); err != nil {
		fmt.Println(err)
	}
}
